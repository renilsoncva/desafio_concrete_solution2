package com.example.renilson.desafio_concrete_solution;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.renilson.desafio_concrete_solution.adapter.FeedItem;
import com.example.renilson.desafio_concrete_solution.adapter.MyRecyclerAdapter_Repo;
import com.example.renilson.desafio_concrete_solution.adapter.RecyclerViewOnClickListenerHack;
import com.example.renilson.desafio_concrete_solution.interfaces.repositorioService;
import com.example.renilson.desafio_concrete_solution.model.RepositorioPull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class lista_repositorio extends AppCompatActivity implements RecyclerViewOnClickListenerHack {


    private List<FeedItem> feedsList;
    private RecyclerView mRecyclerView;
    private MyRecyclerAdapter_Repo adapter;
    private ProgressBar progressBar;
    private LinearLayoutManager mLayoutManager;
    private String param_owner;
    private String param_repo;
    private int page;
    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_lista_repositorio);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        //pegando os parâmetros de busca da tela principal

        Intent intent = getIntent();
        Bundle args = intent.getExtras();
        param_owner = args.getString("owner");
        param_repo = args.getString("repositorio");
//        Log.i("Owner", param_owner);
//        Log.i("Repo", param_repo);
        getSupportActionBar().setTitle(param_repo);

        // Initialize recycler view
        //View view = inflater.inflate(R.layout.act_tela_principal, null);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view_repo);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                final MyRecyclerAdapter_Repo adapter = (MyRecyclerAdapter_Repo) mRecyclerView.getAdapter();

                if(feedsList.size()== llm.findLastCompletelyVisibleItemPosition()+1){
                    ///bloqueia scroll

                    if(mRecyclerView.isNestedScrollingEnabled()) {
                        mRecyclerView.setNestedScrollingEnabled(false);
                    }

                    progressBar.setVisibility(View.VISIBLE);
                    page = page+1;
                    String pageAux = Integer.toString(page);
//                    Toast.makeText(TelaPrincipal.this, "Carregando página: " + pageAux, Toast.LENGTH_SHORT).show();

                    //carregando a lista de repositorios  /////////////////////////////////////////////////////////////////////////
                    carregaLista(Integer.toString(page), adapter);
                }
            }
        });

        // usando o linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // inicializa a progressBar
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        retrofit = new Retrofit.Builder()
                .baseUrl(repositorioService.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        page = page + 1;

        /// verificando o estado da tela

        if (savedInstanceState != null){
            RepositorioListaPull lis = (RepositorioListaPull) savedInstanceState.getSerializable(RepositorioListaPull.KEY);
            feedsList = lis.feedsListP;
            page =lis.pageP;
            progressBar.setVisibility(View.GONE);

            //setando o RecyclerView
            //setando o RecyclerView
            adapter = new MyRecyclerAdapter_Repo(lista_repositorio.this, feedsList);
            adapter.setRecyclerViewOnClickListenerHack(lista_repositorio.this);
            mRecyclerView.setAdapter(adapter);

        }

        if (feedsList == null) {

            //carregando a lista de repositorios  /////////////////////////////////////////////////////////////////////////
            repositorioService service = retrofit.create(repositorioService.class);

            Call<List<RepositorioPull>> resposta = service.repoContributors(param_owner, param_repo, "1");

            resposta.enqueue(new Callback<List<RepositorioPull>>() {
                @Override
                public void onResponse(Call<List<RepositorioPull>> call, Response<List<RepositorioPull>> response) {
                    if (response.isSuccessful()) {
                        feedsList = new ArrayList<>();
                        for (RepositorioPull r : response.body()) {

                            FeedItem item = new FeedItem();
                            if(r.getUser() != null){
                                if (r.getUser().getAvatarUrl()!= null){
                                    item.setAvatar_url(r.getUser().getAvatarUrl());
                                }
                            }

                            if(r.getUser() != null){
                                if(r.getUser().getLogin()!= null){
                                    item.setNome(r.getUser().getLogin());
                                }
                            }

                            if(r.getTitle() != null) {
                                item.setTitle(r.getTitle());
                            }
                            if(r.getCreatedAt() != null){
                                item.setCreated_at(r.getCreatedAt());
                            }

                            if(r.getBody() != null){
                                item.setBody(r.getBody());
                            }

                            if(r.getHtmlUrl() != null){
                                item.setUrl_page(r.getHtmlUrl());
                            }
                            item.setUrl_page(r.getHtmlUrl());
                            if (r.getHead().getRepo() != null){
                                item.setRepositorio(r.getHead().getRepo().getName());
                            }


                            feedsList.add(item);
                        }
                        //Finaliza a progressBar
                        progressBar.setVisibility(View.GONE);
                        //setando o RecyclerView
                        adapter = new MyRecyclerAdapter_Repo(lista_repositorio.this, feedsList);
                        adapter.setRecyclerViewOnClickListenerHack(lista_repositorio.this);
                        mRecyclerView.setAdapter(adapter);

                    } else {
                        Log.i("Erro", "erro");
                        if (page > 0) {
                            page = page - 1;
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<RepositorioPull>> call, Throwable t) {
                    Log.d("Pasha", "No succsess message ");
                }

            });
        }

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);

        return (true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Id correspondente ao botão Up/Home da actionbar
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.voltar:
                Intent i = new Intent(this, TelaPrincipal.class);
                finish();
                startActivity(i);

        }
        return super.onOptionsItemSelected(item);
    }


    public void carregaLista(String page, MyRecyclerAdapter_Repo adapterAux){
        //carregando a lista de repositorios  /////////////////////////////////////////////////////////////////////////

        repositorioService service = retrofit.create(repositorioService.class);

        final Call<List<RepositorioPull>> resposta = service.repoContributors( param_owner,param_repo, page);

        new Thread(){

            @Override
            public void run(){
                super.run();
                try {
                    List<RepositorioPull> response = resposta.execute().body();
                    if (!response.isEmpty() ) {

                        for(RepositorioPull r: response){

                            FeedItem item = new FeedItem();

                            if(r.getUser() != null){
                                if (r.getUser().getAvatarUrl()!= null){
                                    item.setAvatar_url(r.getUser().getAvatarUrl());
                                }
                            }

                            if(r.getUser() != null){
                                if(r.getUser().getLogin()!= null){
                                    item.setNome(r.getUser().getLogin());
                                }
                            }

                            if(r.getTitle() != null) {
                                item.setTitle(r.getTitle());
                            }
                            if(r.getCreatedAt() != null){
                                item.setCreated_at(r.getCreatedAt());
                            }

                            if(r.getBody() != null){
                                item.setBody(r.getBody());
                            }

                            if(r.getHtmlUrl() != null){
                                item.setUrl_page(r.getHtmlUrl());
                            }
                            item.setUrl_page(r.getHtmlUrl());
                            if (r.getHead().getRepo() != null){
                                item.setRepositorio(r.getHead().getRepo().getName());
                            }

                            adapter.addListItem( item, feedsList.size());
                        }
                    }
                } catch (IOException e ){
                    // handle error
                    Log.e("Error", e.getMessage().toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("Carregou a página", "Carregou a página");
                        //Finaliza a progressBar
                        progressBar.setVisibility(View.GONE);
                        if(!mRecyclerView.isNestedScrollingEnabled()) {
                            mRecyclerView.setNestedScrollingEnabled(true);
                        }
                    }
                });
            }
        }.start();
    }


    @Override
    public void onClickListener(View view, int position) {

        Context context = view.getContext();
        Intent i = new Intent(context, page_repositorio.class);
        Bundle params = new Bundle();
        params.putString("urlPage",feedsList.get(position).getUrl_page());
        params.putString("repositorio",feedsList.get(position).getRepositorio());
        params.putString("owner", param_owner);
        params.putString("RepositorioList", param_repo);
        i.putExtras(params);
        finish();
        startActivity(i);
    }

    @Override
    public void onSaveInstanceState(Bundle outStatep){
        super.onSaveInstanceState(outStatep);
        outStatep.putSerializable(RepositorioListaPull.KEY, new RepositorioListaPull(feedsList,page ));
    }

}
