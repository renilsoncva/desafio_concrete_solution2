package com.example.renilson.desafio_concrete_solution;

import com.example.renilson.desafio_concrete_solution.adapter.FeedItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Renilson on 23/05/2016.
 */
public class RepositorioListaPull implements Serializable {
    public List<FeedItem> feedsListP;
    public int pageP;
    public final static String KEY = "RepositorioListaPull";

    public RepositorioListaPull(List<FeedItem> feedsListAuxP, int pageAuxP){
        this.feedsListP = feedsListAuxP;
        this.pageP = pageAuxP;
    }
}