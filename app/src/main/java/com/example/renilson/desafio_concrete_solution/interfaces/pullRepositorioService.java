package com.example.renilson.desafio_concrete_solution.interfaces;

import com.example.renilson.desafio_concrete_solution.model.Repositorio;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Renilson on 20/05/2016.
 */
public interface pullRepositorioService {
    public static final  String baseUrl = "https://api.github.com/" ;
/*
    @GET("repos/{owner}/{repo}/contributors")
    Call<List<itens_Por_Repositorio>> repoContributors(
            @Path("owner") String owner,
            @Path("repo") String repo);
*/
    @GET("search/repositories?q=language:Java&sort=stars")
    Call<Repositorio> listRepos(@Query("page") String page);
}
