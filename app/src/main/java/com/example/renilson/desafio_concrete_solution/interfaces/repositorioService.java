package com.example.renilson.desafio_concrete_solution.interfaces;

import com.example.renilson.desafio_concrete_solution.model.Repositorio;
import com.example.renilson.desafio_concrete_solution.model.RepositorioPull;
import com.example.renilson.desafio_concrete_solution.model.itensTeste;
import com.example.renilson.desafio_concrete_solution.model.itens_Por_Repositorio;
import com.example.renilson.desafio_concrete_solution.model.listaRepositorio;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Renilson on 17/05/2016.
 */
public interface repositorioService {
    public static final  String baseUrl = "https://api.github.com/" ;

    @GET("repos/{owner}/{repo}/pulls")
    Call<List<RepositorioPull>> repoContributors(
            @Path("owner") String owner,
            @Path("repo") String repo,
            @Query("page") String page);
/*
    @GET("search/repositories?q=language:Java&sort=stars&page=1")
    Call<Repositorio> listRepos();

    @GET("search/repositories")
        //@Query("q")  String query
    Call<Repositorio> listRepos2(@Query("q")  String query);

    @GET("search/repositories")
    Call<listaRepositorio> getRepositorio(
            @QueryMap Map<String, String> options
    );

*/

}
